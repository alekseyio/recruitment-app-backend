const authRoutes = require('../routes/authRoutes');
const directorRoutes = require('../routes/directorRoutes');
const requestsRoutes = require('../routes/requestsRoutes');
const personRoutes = require('../routes/personRoutes');
const interviewRoutes = require('../routes/interviewRoutes');
const employeeRoutes = require('../routes/employeeRoutes');

module.exports = app => {
  /**
   * Apply auth routes
   */
  app.use('/api/auth', authRoutes);

  /**
   * Apply director routes
   */
  app.use('/api/director', directorRoutes);

  /**
   * Apply requests routes
   */
  app.use('/api/requests', requestsRoutes);

  /**
   * Apply person routes
   */
  app.use('/api/person', personRoutes);

  /**
   * Apply interview routes
   */
  app.use('/api/interviews', interviewRoutes);

  /**
   * Apply employee routes
   */
  app.use('/api/employees', employeeRoutes);
};
