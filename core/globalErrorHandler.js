const { JsonWebTokenError } = require('jsonwebtoken');

const { ValidationException } = require('../errors/Validation.exception');
const { NotFoundException } = require('../errors/NotFound.exception');
const { BadRequestException } = require('../errors/BadRequest.exception');

/**
 * Send response with validation error
 */
const sendValidationError = ({ errors }, res) =>
  res.status(400).json({ errors });

/**
 * Send response with generic error
 */
const sendGenericError = ({ name, message, stack }, res) => {
  if (process.env.NODE_ENV === 'development') {
    return res.status(500).json({ name, message, stack });
  }

  return res.status(500).send('Произошла внутренняя ошибка сервера');
};

/**
 * Send response that provided token is invalid
 */
const sendTokenInvalidError = res =>
  res.status(400).json({ message: 'Ошибка аутентификации' });

/**
 * Send not found response
 */
const sendNotFoundResponse = ({ message }, res) =>
  res.status(404).json({ statusCode: 404, message });

const sendBadRequestResponse = ({ message, statusCode }, res) => {
  const response = { statusCode };

  if (message) {
    response.message = message;
  }

  return res.status(statusCode).json(response);
};

/**
 * Global error handler responsible for sending appropriate error responses
 */
module.exports = (err, req, res, next) => {
  if (err instanceof ValidationException) {
    return sendValidationError(err, res);
  }

  if (err instanceof JsonWebTokenError) {
    return sendTokenInvalidError(res);
  }

  if (err instanceof NotFoundException) {
    return sendNotFoundResponse(err, res);
  }

  if (err instanceof BadRequestException) {
    return sendBadRequestResponse(err, res);
  }

  return sendGenericError(err, res);
};
