const morgan = require('morgan');
const { json } = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');

module.exports = app => {
  /**
   * Morgan logger only for development purposes
   */
  if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

  /**
   * Cors
   */
  app.use(cors());

  /**
   * Parse incoming requests body
   */
  app.use(json());

  /**
   * Parse incoming requests cookies
   */
  app.use(cookieParser());
};
