class ValidationException extends Error {
  constructor(errors) {
    super();

    this.errors = errors;
  }
}

exports.ValidationException = ValidationException;
