class BadRequestException extends Error {
  constructor(message = '') {
    super();
    this.message = message;
    this.statusCode = 400;
  }
}

exports.BadRequestException = BadRequestException;
