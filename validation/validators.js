const User = require('../models/User');

/**
 * Check if string is alpha + spaces
 */
exports.isAlpha = val => {
  const pattern = /^[а-яА-Яa-zA-Z ]*$/gm;

  return pattern.test(val);
};

/**
 * Check if string is alpha + spaces + numbers
 */
exports.isAlphaNumeric = val => {
  const pattern = /^[а-яА-Яa-zA-Z0-9 ]*$/gm;

  return pattern.test(val);
};

/**
 * Check if email is already taken
 */
exports.isEmailTaken = async email => {
  const user = await User.findOne({ email });

  if (user) {
    throw new Error('Данный адрес электронной почты уже зарегистрирован');
  }

  return true;
};
