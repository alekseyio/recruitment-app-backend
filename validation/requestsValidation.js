const { body } = require('express-validator');

const { isAlphaNumeric, isAlpha } = require('./validators');

const validations = {
  /**
   * New request validation
   */
  createRequest: [
    body('title')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Заголовок не может быть пустым')
      .custom(isAlphaNumeric)
      .withMessage(
        'Заголовок может включать только буквы русского и английского алфавита, цифры'
      )
      .isLength({ min: 4, max: 100 })
      .withMessage('Заголовок может включать от 4 до 100 символов'),

    body('position')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Должность не может быть пустой')
      .custom(isAlpha)
      .withMessage(
        'Должность может включать только буквы русского и английского алфавита'
      ),

    body('schedule')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('График работы не может быть пустым'),

    body('rate')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Ставка обязательна к заполнению')
      .isNumeric({ no_symbols: true })
      .withMessage('Ставка может содержать только цифры'),

    body('qty')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Поле "количество" не может быть пустым')
      .isNumeric({ no_symbols: true })
      .withMessage('Поле "количество" может содержать только цифры'),

    body('comment')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Комментарий обязателен к заполнению')
      .isLength({ max: 255 })
      .withMessage('Комментарий может содержать вплоть до 255 символов'),
  ],

  /**
   * Create interview validation
   */
  createInterview: [
    body('personId')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Соискатель является обязательным полем')
      .isMongoId()
      .withMessage('Идентификатор соискателя имеет неправильный формат'),

    body('interviewDate')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Дата собеседования обязательна к заполнению')
      .isISO8601()
      .withMessage('Дата собеседования имеет неправильный формат'),
  ],

  /**
   * Create interview & person validation
   */
  createInterviewWithPerson: [
    body('firstName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Имя соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Имя соискателя может включать только русские и латинские буквы'
      ),

    body('lastName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Фамилия соискателя не может быть пустой')
      .custom(isAlpha)
      .withMessage(
        'Фамилия соискателя может включать только русские и латинские буквы'
      ),

    body('middleName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Отчество соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Отчество соискателя может включать только русские и латинские буквы'
      ),

    body('age')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Возраст соискателя обязателен к заполнению')
      .isNumeric({ no_symbols: true })
      .withMessage('Возраст соискателя может содержать только цифры'),

    body('nationality')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Национальность обязательна к заполнению')
      .custom(isAlpha)
      .withMessage('Национальность может содержать только буквы'),

    body('phone')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Телефонный номер обязателен к заполнению')
      .isMobilePhone('ru-RU')
      .withMessage(
        'Телефонный номер должен быть в формате "8ХХХХХХХХХХ" или "+7ХХХХХХХХХХ"'
      ),

    body('comment')
      .if(val => typeof val !== 'undefined' && val.length)
      .trim()
      .escape(),

    body('interviewDate')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Дата собеседования обязательна к заполнению')
      .isISO8601()
      .withMessage('Дата собеседования имеет неправильный формат'),
  ],
};

module.exports = validator => validations[validator];
