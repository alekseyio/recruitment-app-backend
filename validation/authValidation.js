const { body } = require('express-validator');

const { isAlpha, isEmailTaken } = require('./validators');
const { capitalize } = require('./sanitizers');
const { roles } = require('../constants');

const validations = {
  /**
   * Register validation
   */
  register: [
    body('firstName')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Имя" обязательно к заполнению')
      .custom(isAlpha)
      .withMessage('Поле "Имя" должно содержать только буквы и пробелы')
      .isLength({ min: 2, max: 50 })
      .withMessage('Поле "Имя" должно содержать от 2 до 50 знаков')
      .customSanitizer(capitalize),

    body('lastName')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Фамилия" обязательно к заполнению')
      .custom(isAlpha)
      .withMessage('Поле "Фамилия" должно содержать только буквы и пробелы')
      .isLength({ min: 2, max: 50 })
      .withMessage('Поле "Фамилия" должно содержать от 2 до 50 знаков')
      .customSanitizer(capitalize),

    body('middleName')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Отчество" обязательно к заполнению')
      .custom(isAlpha)
      .withMessage('Поле "Отчество" должно содержать только буквы и пробелы')
      .isLength({ min: 2, max: 50 })
      .withMessage('Поле "Отчество" должно содержать от 2 до 50 знаков')
      .customSanitizer(capitalize),

    body('email')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Адрес электронной почты" обязательно к заполнению')
      .isEmail({
        allow_utf8_local_part: false,
        ignore_max_length: false,
        domain_specific_validation: true
      })
      .withMessage('Адрес электронной почты указан неверно')
      .normalizeEmail()
      .custom(isEmailTaken),

    body('password')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Пароль" обязательно к заполнению')
      .isAlphanumeric()
      .withMessage('Поле "Пароль" должно содержать только буквы и цифры')
      .isLength({ min: 8, max: 32 })
      .withMessage('Поле "Пароль" должно содержать от 8 до 32 знаков')
      .not()
      .matches(/(pass|password|qwerty|1234)/i)
      .withMessage('Не используйте простые пароли'),

    body('passwordConfirmation')
      .if(
        (_, { req }) =>
          typeof req.body.password !== 'undefined' &&
          req.body.password.length >= 8
      )
      .escape()
      .trim()
      .custom(
        (passwordConfirmation, { req }) =>
          req.body.password === passwordConfirmation
      )
      .withMessage('Пароли не совпадают'),

    body('role')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "должность" не может быть пустым')
      .custom(role => roles.includes(role))
      .withMessage('Данной должности нет в системе')
  ],

  /**
   * Login validation
   */
  login: [
    body('email')
      .escape()
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "Адрес электронной почты" обязательно к заполнению')
      .isEmail({
        allow_utf8_local_part: false,
        ignore_max_length: false,
        domain_specific_validation: true
      })
      .withMessage('Адрес электронной почты введён неверно')
      .normalizeEmail(),

    body('password')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Поле "Пароль" обязательно к заполнению')
  ]
};

module.exports = validator => validations[validator];
