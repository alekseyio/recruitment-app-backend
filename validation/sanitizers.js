/**
 * Capitalize a string
 */
exports.capitalize = val => {
  if (typeof val !== 'string')
    throw new Error('Данное поле может содержать только строки');

  return val.slice(0, 1).toUpperCase() + val.slice(1).toLowerCase();
};
