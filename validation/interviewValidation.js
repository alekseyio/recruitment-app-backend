const { body, param } = require('express-validator');
const { isAlpha } = require('./validators');

const validations = {
  /**
   * Get ALL interviews for a given request
   */
  getAllInterviewsForRequest: [
    param('requestId')
      .isMongoId()
      .withMessage('Id заявки не соответствует заданному формату'),
  ],

  /**
   * Get interview by Id validation
   */
  getInterviewById: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),
  ],

  /**
   * Create interview with person from DB validation
   */
  createInterview: [
    body('requestId')
      .trim()
      .escape()
      .isMongoId()
      .withMessage('Id заявки имеет неправильный формат'),

    body('personId')
      .trim()
      .escape()
      .isMongoId()
      .withMessage('Id соискателя имеет неправильный формат'),

    body('date')
      .trim()
      .escape()
      .isISO8601()
      .withMessage('Дата собеседования имеет неправильный формат')
      .toDate(),
  ],

  /**
   * Create interview & person
   */
  createInterviewAndPerson: [
    body('firstName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Имя соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Имя соискателя может включать только русские и латинские буквы'
      ),

    body('lastName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Фамилия соискателя не может быть пустой')
      .custom(isAlpha)
      .withMessage(
        'Фамилия соискателя может включать только русские и латинские буквы'
      ),

    body('middleName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Отчество соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Отчество соискателя может включать только русские и латинские буквы'
      ),

    body('age')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Возраст соискателя обязателен к заполнению')
      .isNumeric({ no_symbols: true })
      .withMessage('Возраст соискателя может содержать только цифры')
      .toInt(),

    body('phone')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Телефонный номер обязателен к заполнению')
      .isMobilePhone('ru-RU')
      .withMessage(
        'Телефонный номер должен быть в формате "8ХХХХХХХХХХ" или "+7ХХХХХХХХХХ"'
      ),

    body('citizenship')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Гражданство обязательно к заполнению')
      .custom(isAlpha)
      .withMessage(
        'Гражданство может включать только русские и латинские буквы'
      ),

    body('comment')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Комментарий о соискателе обязателен к заполнению'),

    body('requestId')
      .trim()
      .escape()
      .isMongoId()
      .withMessage('Id заявки имеет неправильный формат'),

    body('date')
      .trim()
      .escape()
      .isISO8601()
      .withMessage('Дата собеседования имеет неправильный формат')
      .toDate(),

    body('resource')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "ресурс" обязательно к заполнению'),
  ],

  /**
   * Delete interview validation
   */
  deleteInterview: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),
  ],

  /**
   * Change interview date
   */
  changeInterviewDate: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),

    body('newDate')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Новая дата собеседования не может быть пустой')
      .isISO8601()
      .withMessage('Новая дата собеседования имеет неправильный формат')
      .toDate(),
  ],

  /**
   * Get available candidates
   */
  getAvailableCandidates: [
    param('requestId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),
  ],

  /**
   * Get interview & request for director
   */
  getInterviewForDirectorsRequest: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),
  ],

  /**
   * Set interview result
   */
  setInterviewResult: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),

    body('result')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Результат собеседования обязателен к заполнению')
      .isIn(['fail', 'success'])
      .withMessage(
        'Результат собеседования может иметь следующие значения: "Принят на работу", "Не принят на работу"'
      ),

    body('comment')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Результат собеседования обязателен к заполнению'),
  ],
};

module.exports = validator => validations[validator];
