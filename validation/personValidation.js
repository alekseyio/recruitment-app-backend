const { body } = require('express-validator');

const { isAlpha } = require('./validators');

const validations = {
  /**
   * Create person validation
   */
  createPerson: [
    body('firstName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Имя соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Имя соискателя может включать только русские и латинские буквы'
      ),

    body('lastName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Фамилия соискателя не может быть пустой')
      .custom(isAlpha)
      .withMessage(
        'Фамилия соискателя может включать только русские и латинские буквы'
      ),

    body('middleName')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Отчество соискателя не может быть пустым')
      .custom(isAlpha)
      .withMessage(
        'Отчество соискателя может включать только русские и латинские буквы'
      ),

    body('age')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Возраст соискателя обязателен к заполнению')
      .isNumeric({ no_symbols: true })
      .withMessage('Возраст соискателя может содержать только цифры')
      .toInt(),

    body('phone')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Телефонный номер обязателен к заполнению')
      .isMobilePhone('ru-RU')
      .withMessage(
        'Телефонный номер должен быть в формате "8ХХХХХХХХХХ" или "+7ХХХХХХХХХХ"'
      ),

    body('citizenship')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Гражданство обязательно к заполнению')
      .custom(isAlpha)
      .withMessage(
        'Гражданство может включать только русские и латинские буквы'
      ),

    body('comment')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Комментарий о соискателе обязателен к заполнению'),

    body('resource')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Поле "ресурс" обязательно к заполнению'),
  ],
};

module.exports = validator => validations[validator];
