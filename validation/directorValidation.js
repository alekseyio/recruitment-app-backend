const { body, param } = require('express-validator');

const validations = {
  /**
   * Add company validation
   */
  addCompany: [
    body('brand')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Название бренда не может быть пустым')
      .isLength({ min: 2, max: 100 })
      .withMessage('Название бренда может содержать от 2 до 100 символов'),

    body('objectNumber')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Номер объекта не может быть пустым')
      .isNumeric({ no_symbols: true })
      .withMessage('Номер объекта может содержать только цифры')
      .toInt()
      .custom(val => {
        if (val === 0) throw new Error();

        return true;
      })
      .withMessage('Номер объекта не может быть меньше или равен 0'),

    body('subway')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Станция метро не может быть пустой'),

    body('subwayLine')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Линия станции метро не может быть пустой')
      .isNumeric({ no_symbols: true })
      .withMessage('Линия станции метро может включать только цифры')
      .toInt()
      .custom(val => {
        if (val < 1) throw new Error();

        return true;
      })
      .withMessage('Линия станции метро не может быть меньше или равна 0'),

    body('address')
      .trim()
      .not()
      .isEmpty()
      .withMessage('Адрес не может быть пустым'),
  ],

  /**
   * Get interview validation
   */
  getInterviewById: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),
  ],

  /**
   * Set interview result validation
   */
  setInterviewResult: [
    param('interviewId')
      .isMongoId()
      .withMessage('Id собеседования имеет неправильный формат'),

    body('result')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Результат собеседования обязателен к заполнению')
      .isIn(['success', 'fail'])
      .withMessage('Неверное значение результата собеседования'),

    body('comment')
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage('Комментарий обязателен к заполнению'),
  ],
};

module.exports = validator => validations[validator];
