const { Router } = require('express');

const RequestsController = require('../controllers/RequestsController');
const catchError = require('../helpers/catchError');
const authorize = require('../middleware/authorize');
const only = require('../middleware/only');
const validate = require('../validation/requestsValidation');

const router = Router();

/**
 * Get all requests
 */
router.get(
  '/',
  authorize,
  only('recruiter', 'director'),
  catchError(RequestsController.getRequests)
);

/**
 * Get all requests by user
 */
router.get(
  '/user/:userId',
  authorize,
  only('recruiter', 'director'),
  catchError(RequestsController.getRequestsByUser)
);

/**
 * Get all new requests
 */
router.get(
  '/new',
  authorize,
  only('recruiter'),
  catchError(RequestsController.getNewRequests)
);

/**
 * Get all requests that are being processed
 */
router.get(
  '/processing',
  authorize,
  only('recruiter'),
  catchError(RequestsController.getAllProcessingRequests)
);

/**
 * Get request by id
 */
router.get(
  '/:id',
  authorize,
  only('recruiter', 'director'),
  catchError(RequestsController.getRequestById)
);

/**
 * Get new requests count
 */
router.get(
  '/new/count',
  authorize,
  only('recruiter'),
  catchError(RequestsController.getNewRequestsCount)
);

/**
 * Get new request by id
 */
router.get(
  '/new/:requestId',
  authorize,
  only('recruiter'),
  catchError(RequestsController.getNewRequestById)
);

/**
 * Get processing request by id
 */
router.get(
  '/processing/:requestId',
  authorize,
  only('recruiter'),
  catchError(RequestsController.getProcessingRequestById)
);

/**
 * Create new request
 */
router.post(
  '/',
  authorize,
  only('director'),
  validate('createRequest'),
  catchError(RequestsController.createRequest)
);

/**
 * Delete request by id
 */
router.delete(
  '/:requestId',
  authorize,
  only('recruiter', 'director'),
  catchError(RequestsController.deleteRequest)
);

/**
 * Change request status to 'processing'
 */
router.patch(
  '/new/:requestId',
  authorize,
  only('recruiter'),
  catchError(RequestsController.setRequestStatus('processing'))
);

/**
 * Change request status to 'new
 */
router.patch(
  '/processing/:requestId',
  authorize,
  only('recruiter'),
  catchError(RequestsController.setRequestStatus('new'))
);

/**
 * Create new interview
 */
router.post(
  '/processing/:id/interviews',
  authorize,
  only('recruiter'),
  validate('createInterview'),
  catchError(RequestsController.createInterview)
);

/**
 * Create new interview & person
 */
router.post(
  '/processing/:id/interviews/person',
  authorize,
  only('recruiter'),
  validate('createInterviewWithPerson'),
  catchError(RequestsController.createInterviewWithPerson)
);

module.exports = router;
