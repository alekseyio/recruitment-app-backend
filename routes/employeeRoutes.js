const { Router } = require('express');

const EmployeeController = require('../controllers/EmployeeController');
const authorize = require('../middleware/authorize');
const only = require('../middleware/only');
const catchError = require('../helpers/catchError');

const router = Router();

/**
 * Attach employee to object by object number
 */
router.post(
  '',
  authorize,
  only('recruiter'),
  catchError(EmployeeController.attachEmployeeToCompany)
);

/**
 * Get all employees
 */
router.get(
  '/',
  authorize,
  only('director'),
  catchError(EmployeeController.getEmployees)
);

module.exports = router;
