const { Router } = require('express');

const DirectorController = require('../controllers/DirectorController');
const catchError = require('../helpers/catchError');
const authorize = require('../middleware/authorize');
const only = require('../middleware/only');
const validate = require('../validation/directorValidation');

const router = Router();

/**
 * Associate company with director
 */
router.post(
  '/add-company',
  authorize,
  only('director'),
  validate('addCompany'),
  catchError(DirectorController.addCompany)
);

/**
 * Delete request associated with director
 */
router.delete(
  '/requests/:id',
  authorize,
  only('director'),
  catchError(DirectorController.deleteRequest)
);

/**
 * Get interview count
 */
router.get(
  '/interviews/count',
  authorize,
  only('director'),
  catchError(DirectorController.getInterviewCount)
);

/**
 * Get interview by id
 */
router.get(
  '/interviews/:interviewId',
  authorize,
  only('director'),
  validate('getInterviewById'),
  catchError(DirectorController.getInterviewById)
);

/**
 * Set interview result
 */
router.patch(
  '/interviews/:interviewId/result',
  authorize,
  only('director'),
  validate('setInterviewResult'),
  catchError(DirectorController.setInterviewResult)
);

module.exports = router;
