const { Router } = require('express');

const catchError = require('../helpers/catchError');
const PersonController = require('../controllers/PersonController');
const authorize = require('../middleware/authorize');
const only = require('../middleware/only');
const validate = require('../validation/personValidation');

const router = Router();

/**
 * Get all people
 */
router.get(
  '/',
  authorize,
  only('recruiter'),
  catchError(PersonController.getAllPeople)
);

/**
 * Get person by id
 */
router.get(
  '/:personId',
  authorize,
  only('recruiter'),
  catchError(PersonController.getPersonById)
);

/**
 * Create new person record
 */
router.post(
  '/',
  authorize,
  only('recruiter'),
  validate('createPerson'),
  catchError(PersonController.createPerson)
);

module.exports = router;
