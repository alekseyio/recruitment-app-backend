const { Router } = require('express');

const InterviewsController = require('../controllers/InterviewsController');
const catchError = require('../helpers/catchError');
const authorize = require('../middleware/authorize');
const only = require('../middleware/only');
const validate = require('../validation/interviewValidation');

const router = Router();

/**
 * Get all interviews
 */
router.get(
  '/',
  authorize,
  only('director', 'recruiter'),
  catchError(InterviewsController.getAllInterviews)
);

/**
 * Get active interviews
 */
router.get(
  '/active',
  authorize,
  only('director', 'recruiter'),
  catchError(InterviewsController.getActiveInterviews)
);

/**
 * Get ALL interviews for a given request
 */
router.get(
  '/request/:requestId',
  authorize,
  only('director', 'recruiter'),
  validate('getAllInterviewsForRequest'),
  catchError(InterviewsController.getAllInterviewsForRequest)
);

/**
 * Get all active interviews for a given request
 */
router.get(
  '/request/:requestId/active',
  authorize,
  only('director', 'recruiter'),
  catchError(InterviewsController.getActiveInterviewsForRequest)
);

/**
 * Get all interviews for authenticated director requests
 */
router.get(
  '/director',
  authorize,
  only('director'),
  catchError(InterviewsController.getDirectorInterviews)
);

/**
 * Get an interview & request for a director
 */
router.get(
  '/:interviewId/director',
  authorize,
  only('director'),
  validate('getInterviewForDirectorsRequest'),
  catchError(InterviewsController.getInterviewForDirectorsRequest)
);

/**
 * Get any interview by Id, only for recruiter
 */
router.get(
  '/:interviewId',
  authorize,
  only('recruiter'),
  validate('getInterviewById'),
  catchError(InterviewsController.getInterviewById)
);

/**
 * Create new interview with person from DB
 */
router.post(
  '/',
  authorize,
  only('recruiter'),
  validate('createInterview'),
  catchError(InterviewsController.createInterview)
);

/**
 * Create new interview & add new person to the DB
 */
router.post(
  '/person',
  authorize,
  only('recruiter'),
  validate('createInterviewAndPerson'),
  catchError(InterviewsController.createInterviewAndPerson)
);

/**
 * Delete interview
 */
router.delete(
  '/:interviewId',
  authorize,
  only('recruiter'),
  validate('deleteInterview'),
  catchError(InterviewsController.deleteInterview)
);

/**
 * Update interview date
 */
router.patch(
  '/:interviewId/date',
  authorize,
  only('recruiter'),
  validate('changeInterviewDate'),
  catchError(InterviewsController.changeInterviewDate)
);

/**
 * Update interview with a result
 */
router.patch(
  '/:interviewId/result',
  authorize,
  only('director'),
  validate('setInterviewResult'),
  catchError(InterviewsController.setInterviewResult)
);

/**
 * Set interview inactive
 */
router.patch(
  '/:interviewId/inactive',
  authorize,
  only('recruiter'),
  catchError(InterviewsController.setInterviewInactive)
);

/**
 * Get available candidates for an interview
 */
router.get(
  '/request/:requestId/candidates',
  authorize,
  only('recruiter'),
  validate('getAvailableCandidates'),
  catchError(InterviewsController.getAvailableCandidates)
);

module.exports = router;
