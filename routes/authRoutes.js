const { Router } = require('express');

const AuthController = require('../controllers/AuthController');
const catchError = require('../helpers/catchError');
const validate = require('../validation/authValidation');
const authorize = require('../middleware/authorize');
const unauthorize = require('../middleware/unauthorize');

const router = Router();

/**
 * Register a new user
 */
router.post(
  '/register',
  unauthorize,
  validate('register'),
  catchError(AuthController.register)
);

/**
 * Login a user
 */
router.post(
  '/login',
  unauthorize,
  validate('login'),
  catchError(AuthController.login)
);

/**
 * Logout a user
 */
router.post('/logout', authorize, catchError(AuthController.logout));

/**
 * Get current user info
 */
router.get('/current', authorize, catchError(AuthController.getCurrentUser));

module.exports = router;
