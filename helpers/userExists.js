const User = require('../models/User');

module.exports = async email => {
  const user = await User.findOne({ email });

  return user;
};
