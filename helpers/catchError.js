/**
 * Wrap handlers with this function
 * which catches any errors
 * and passes them into global error handler
 */
module.exports = cb => (req, res, next) => cb(req, res, next).catch(next);
