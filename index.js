require('dotenv').config();
const { connect } = require('mongoose');

const app = require('./app');

const server = app.listen(process.env.PORT, async () => {
  console.log(`Server started on port: ${process.env.PORT}`);

  try {
    await connect(
      process.env.DB_CONN,
      { useNewUrlParser: true }
    );

    console.log('Database connection established');
  } catch (err) {
    console.log('Failed to connect to the database', err);
  }
});
