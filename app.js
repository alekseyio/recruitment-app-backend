const express = require('express');
const cors = require('cors');

const applyMiddleware = require('./core/applyMiddleware');
const applyRoutes = require('./core/applyRoutes');
const globalErrorHandler = require('./core/globalErrorHandler');

const app = express();

app.use(cors())

/**
 * Apply global middleware
 */
applyMiddleware(app);

/**
 * Apply routes
 */
applyRoutes(app);

/**
 * Apply global error handler
 */
app.use(globalErrorHandler);

module.exports = app;
