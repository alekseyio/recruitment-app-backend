const BaseController = require('./BaseController');
const Request = require('../models/Request');
const Person = require('../models/Person');
const Employee = require('../models/Employee');
const Company = require('../models/Company');
const User = require('../models/User');
const { Interview } = require('../models/Interview');
const { NotFoundException } = require('../errors/NotFound.exception');

class EmployeeController extends BaseController {
  constructor() {
    super();

    this.attachEmployeeToCompany = this.attachEmployeeToCompany.bind(this);
  }

  async attachEmployeeToCompany(req, res) {
    const request = await Request.findById(req.body.requestId).populate('user');
    const director = await User.findById(request.user.id).populate('company');
    const company = await Company.findOne({ brand: director.company.brand });
    const interview = await Interview.findById(req.body.interviewId);
    const person = await Person.findById(req.body.personId);

    if (!request || !interview || !person) throw new NotFoundException();

    const employee = new Employee({
      company: company.id,
      person: person.id,
      position: request.position,
      schedule: request.schedule,
    });

    await employee.save();

    await person.set('employed', true).save();

    await request.set('status', 'closed').save();

    await interview.set('active', false).save();

    return res.status(200).send();
  }

  async getEmployees(req, res, next) {
    if (!req.user.company) {
      return res.status(403).json();
    }

    const company = await Company.findOne({
      objectNumber: req.user.company.objectNumber,
    });

    const employees = await Employee.find({
      company: company.id,
    }).populate('person');

    const response = {
      company: {
        brand: company.brand,
        number: company.objectNumber,
        address: company.address,
      },
      employees,
    };

    return res.status(200).json(response);
  }
}

module.exports = new EmployeeController();
