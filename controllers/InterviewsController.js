const BaseController = require('./BaseController');
const Request = require('../models/Request');
const Person = require('../models/Person');
const { Interview } = require('../models/Interview');
const { BadRequestException } = require('../errors/BadRequest.exception');
const { NotFoundException } = require('../errors/NotFound.exception');

class InterviewsController extends BaseController {
  constructor() {
    super();

    this.getAllInterviewsForRequest = this.getAllInterviewsForRequest.bind(
      this
    );
    this.getInterviewById = this.getInterviewById.bind(this);
    this.createInterview = this.createInterview.bind(this);
    this.createInterviewAndPerson = this.createInterviewAndPerson.bind(this);
    this.deleteInterview = this.deleteInterview.bind(this);
    this.changeInterviewDate = this.changeInterviewDate.bind(this);
    this.getAvailableCandidates = this.getAvailableCandidates.bind(this);
    this.getInterviewForDirectorsRequest = this.getInterviewForDirectorsRequest.bind(
      this
    );
    this.setInterviewResult = this.setInterviewResult.bind(this);
  }

  async getAllInterviews(req, res) {
    const interviews = await Interview.find();

    return res.status(200).json(interviews);
  }

  async getActiveInterviews(req, res) {
    const activeInterviews = await Interview.find({ active: true });

    return res.status(200).json(activeInterviews);
  }

  async getAllInterviewsForRequest(req, res) {
    this.validate(req);

    const request = await Request.findById(req.params.requestId);
    if (!request) throw new NotFoundException('Данной заявки нет в системе');

    const interviews = await Interview.find({ request: req.params.requestId })
      .populate('request')
      .populate('person')
      .sort({ date: 'asc' });

    return res.status(200).json(interviews);
  }

  async getInterviewById(req, res) {
    this.validate(req);

    const interview = await Interview.findById(req.params.interviewId).populate(
      'person'
    );
    if (!interview)
      throw new NotFoundException('Данного собеседования нет в системе');

    return res.status(200).json(interview);
  }

  async createInterview(req, res) {
    this.validate(req);

    const request = await Request.findOne({
      _id: req.body.requestId,
      status: 'processing',
    });
    if (!request) throw new NotFoundException('Данной заявки нет в системе');

    const person = await Person.findOne({
      _id: req.body.personId,
      employed: false,
    });
    if (!person)
      throw new NotFoundException('Данного соискателя нет в системе');

    const interview = await Interview.findOne({
      request,
      person,
    });
    if (interview)
      throw new BadRequestException(
        'Данный соискатель уже находится в списке кандидатов'
      );

    const attributes = {
      request: request.id,
      person: person.id,
      date: req.body.date,
    };
    const newInterview = new Interview(attributes);

    await newInterview.save();

    return res.status(201).json(newInterview);
  }

  async createInterviewAndPerson(req, res) {
    this.validate(req);

    const request = await Request.findOne({
      _id: req.body.requestId,
      status: 'processing',
    });
    if (!request) throw new NotFoundException('Данной заявки нет в системе');

    const person = new Person(
      this.only(req.body, [
        'firstName',
        'lastName',
        'middleName',
        'age',
        'citizenship',
        'phone',
        'comment',
        'resource',
      ])
    );

    await person.save();

    const attributes = {
      request: request.id,
      person: person.id,
      date: req.body.date,
    };
    const newInterview = new Interview(attributes);

    await newInterview.save();

    return res.status(201).json(newInterview);
  }

  async deleteInterview(req, res) {
    this.validate(req);

    const interview = await Interview.findById(req.params.interviewId);
    if (!interview)
      throw new NotFoundException('Данного интервью нет в системе');

    await interview.remove();

    return res.status(200).json({});
  }

  async getDirectorInterviews(req, res) {
    const requests = await Request.find({
      user: req.user.id,
      status: 'processing',
    });

    const interviews = await Interview.find({
      active: true,
      request: { $in: requests.map(r => r.id) },
    })
      .populate('person')
      .populate('request')
      .sort({ date: 'asc' });

    return res.status(200).json(interviews);
  }

  async changeInterviewDate(req, res) {
    this.validate(req);

    const interview = await Interview.findById(req.params.interviewId);
    if (!interview)
      throw new NotFoundException('Данного собеседования нет в системе');

    interview.set('date', req.body.newDate);
    await interview.save();

    return res.status(200).json(interview);
  }

  /**
   * Get available candidates
   */
  async getAvailableCandidates(req, res) {
    this.validate(req);

    const request = await Request.findById(req.params.requestId);
    if (!request) throw new NotFoundException('Данной заявки нет в системе');

    const interviews = await Interview.find({ request: request.id });

    const candidates = await Person.find({
      _id: { $nin: interviews.map(int => int.person) },
      employed: false,
    });

    return res.status(200).json(candidates);
  }

  /**
   * Get interview & request for director
   */
  async getInterviewForDirectorsRequest(req, res) {
    this.validate(req);

    const interview = await Interview.findById(req.params.interviewId)
      .populate('request')
      .populate('person');
    const appRequest = await Request.findById(interview.request.id).populate(
      'user'
    );

    if (req.user.id !== appRequest.user.id) {
      return res.status(403).json({ statusCode: 403, message: 'Forbidden' });
    }

    return res.status(200).json(interview);
  }

  async setInterviewResult(req, res) {
    this.validate(req);

    const interview = await Interview.findById(req.params.interviewId)
      .populate('request')
      .populate('person');
    if (!interview)
      throw new NotFoundException('Данного собеседования нет в системе');

    interview.set({
      result: req.body.result,
      comment: req.body.comment,
    });

    await interview.save();

    return res.status(200).json(interview);
  }

  async setInterviewInactive(req, res) {
    const interview = await Interview.findById(req.params.interviewId);
    if (!interview) throw new NotFoundException();

    await interview.set('active', false).save();

    return res.status(200).json(interview);
  }

  async getActiveInterviewsForRequest(req, res) {
    const interviews = await Interview.find({
      request: req.params.requestId,
      active: true,
    })
      .populate('request')
      .populate('person');

    return res.status(200).json(interviews);
  }
}

module.exports = new InterviewsController();
