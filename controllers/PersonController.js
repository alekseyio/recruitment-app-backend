const BaseController = require('./BaseController');
const Person = require('../models/Person');

class PersonController extends BaseController {
  constructor() {
    super();

    this.createPerson = this.createPerson.bind(this);
  }

  /**
   * Get all people
   */
  async getAllPeople(req, res, next) {
    try {
      const people = await Person.find();

      return res.status(200).json({ people });
    } catch (err) {
      next(err);
    }
  }

  /**
   * Get person by id
   */
  async getPersonById(req, res, next) {
    try {
      const person = await Person.findById(req.params.personId);

      if (!person) {
        return res.status(404).json({
          message: 'Соискателя с таким идентификатором нет в системе',
        });
      }

      return res.status(200).json({ person });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Create new person record
   */
  async createPerson(req, res, next) {
    this.validate(req);

    const person = new Person(
      this.only(req.body, [
        'firstName',
        'lastName',
        'middleName',
        'age',
        'citizenship',
        'phone',
        'comment',
      ])
    );

    await person.save();

    return res.status(201).json(person);
  }
}

module.exports = new PersonController();
