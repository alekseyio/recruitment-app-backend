const BaseController = require('./BaseController');
const Request = require('../models/Request');
const User = require('../models/User');
const Person = require('../models/Person');

class RequestsController extends BaseController {
  constructor() {
    super();

    this.createRequest = this.createRequest.bind(this);
    this.createInterview = this.createInterview.bind(this);
    this.createInterviewWithPerson = this.createInterviewWithPerson.bind(this);
  }

  /**
   * Get all requests
   */
  async getRequests(req, res, next) {
    const requests = await Request.find();

    return res.status(200).json({
      requests
    });
  }

  /**
   * Get all requests by user
   */
  async getRequestsByUser(req, res, next) {
    let user;

    try {
      user = await User.findById(req.params.userId);

      if (!user) {
        return res.status(404).json({
          message: 'Пользователя с таким идентификатором нет в системе'
        });
      }

      const requests = await Request.find({
        user: user.id,
        status: {
          $in: ['new', 'processing']
        }
      });

      return res.status(200).json({ requests });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Get all new requests
   */
  async getNewRequests(req, res, next) {
    const requests = await Request.find({ status: 'new' });

    return res.status(200).json({ requests });
  }

  /**
   * Get request by id
   */
  async getRequestById(req, res, next) {
    try {
      const request = await Request.findById(req.params.id).populate(
        'interviews.person'
      );

      if (!request) {
        return res.status(404).json();
      }

      return res.status(200).json(request);
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Get NEW request by id
   */
  async getNewRequestById(req, res, next) {
    try {
      const request = await Request.findOne({
        status: 'new',
        _id: req.params.requestId
      });

      if (!request) {
        return res
          .status(404)
          .json({ message: 'Заявки с таким идентификатором нет в системе' });
      }

      return res.status(200).json({ request });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Get all requests that are being processed
   */
  async getAllProcessingRequests(req, res, next) {
    const requests = await Request.find({ status: 'processing' });

    return res.status(200).json({ requests });
  }

  async getProcessingRequestById(req, res, next) {
    try {
      const request = await Request.findOne({
        status: 'processing',
        _id: req.params.requestId
      }).populate('interviews.person');

      if (!request) {
        return res
          .status(404)
          .json({ message: 'В системе нет заявки с таким идентификатором' });
      }

      return res.status(200).json({ request });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Create new request
   */
  async createRequest(req, res, next) {
    this.validate(req);

    if (!req.user.company) {
      return res.status(403).json({
        message: 'Данный пользователь не закреплен за каким-либо объектом'
      });
    }

    const newReq = new Request(
      this.only(req.body, [
        'title',
        'position',
        'schedule',
        'rate',
        'qty',
        'comment'
      ])
    );

    newReq.user = req.user.id;

    try {
      await newReq.save();
    } catch (err) {
      return next(err);
    }

    return res.status(201).json({
      request: newReq
    });
  }

  /**
   * Delete request by id
   */
  async deleteRequest(req, res, next) {
    try {
      const request = await Request.findById(req.params.requestId);

      if (!request) {
        return res
          .status(404)
          .json({ message: 'Заявки с таким идентификатором нет в системе' });
      }

      await request.remove();

      return res.status(200).json({ message: 'Заявка успешно удалена' });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Change request status
   */
  setRequestStatus(status) {
    return async function(req, res, next) {
      try {
        const request = await Request.findById(req.params.requestId);

        if (!request) {
          return res
            .status(404)
            .json({ message: 'Заявки с таким идентификатором нет в системе' });
        }

        request.set('status', status);

        if (status === 'processing') {
          request.set('interviews', []);
        } else if (status === 'new') {
          request.set('interviews', undefined);
        }

        await request.save();

        return res.status(200).json({ request });
      } catch (err) {
        return next(err);
      }
    };
  }

  /**
   * Create interview
   */
  async createInterview(req, res, next) {
    this.validate(req);

    try {
      const request = await Request.findOne({
        _id: req.params.id,
        status: 'processing'
      });

      if (!request) {
        return res
          .status(404)
          .json({ message: 'В системе нет заявки с таким идентификатором' });
      }

      const person = await Person.findById(req.body.personId);

      if (!person) {
        return res.status(404).json({
          message: 'В системе нет соискателя с таким идентификатором'
        });
      }

      if (person.employed) {
        return res
          .status(400)
          .json({ message: 'Данный соискатель уже трудоустроен' });
      }

      for (let interview of request.interviews) {
        if (interview.person.toString() === person.id) {
          return res.status(400).json({
            message: 'Данный соискатель уже находится в списке кандидатов'
          });
        }
      }

      request.set('interviews', [
        ...request.interviews,
        {
          person,
          interviewDate: req.body.interviewDate
        }
      ]);

      await request.save();

      return res.status(201).json({ request });
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Create interview & person
   */
  async createInterviewWithPerson(req, res, next) {
    try {
      this.validate(req);

      const request = await Request.findOne({
        _id: req.params.id,
        status: 'processing'
      });

      if (!request) {
        return res.status(404).json({
          message: 'В системе нет заявки с таким идентификатором'
        });
      }

      const person = new Person(
        this.only(req.body, [
          'firstName',
          'lastName',
          'middleName',
          'age',
          'nationality',
          'phone',
          'comment'
        ])
      );

      await person.save();

      request.set('interviews', [
        ...request.interviews,
        {
          person,
          interviewDate: req.body.interviewDate
        }
      ]);

      await request.save();

      return res.status(201).json({
        request
      });
    } catch (err) {
      return next(err);
    }
  }

  async getNewRequestsCount(req, res, next) {
    const requests = await Request.find({
      status: 'new'
    });

    return res.status(200).json(requests.length);
  }
}

module.exports = new RequestsController();
