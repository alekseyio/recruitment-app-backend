const BaseController = require('./BaseController');
const Company = require('../models/Company');
const Request = require('../models/Request');
const { Interview } = require('../models/Interview');
const { NotFoundException } = require('../errors/NotFound.exception');

class DirectorController extends BaseController {
  constructor() {
    super();

    this.addCompany = this.addCompany.bind(this);
    this.getInterviewById = this.getInterviewById.bind(this);
    this.setInterviewResult = this.setInterviewResult.bind(this);
  }

  /**
   * Associate company with director if it doesn't have one
   */
  async addCompany(req, res, next) {
    this.validate(req);

    if (req.user.company) {
      return res.status(400).json({
        message: 'Данный пользователь уже закреплен за определенным объектом'
      });
    }

    const company = new Company(
      this.only(req.body, [
        'brand',
        'objectNumber',
        'address',
        'subway',
        'subwayLine'
      ])
    );

    try {
      await company.save();
    } catch (err) {
      return next(err);
    }

    req.user.addCompany(company.id);

    return res.status(201).json({
      message: 'Объект был успешно закреплен за вашей учетной записью'
    });
  }

  /**
   * Delete request associated with director
   */
  async deleteRequest(req, res, next) {
    try {
      const request = await Request.findOne({
        _id: req.params.id,
        user: req.user.id
      });

      if (!request) {
        return res.status(404).json();
      }

      await request.remove();

      return res.status(204).json({});
    } catch (err) {
      return next(err);
    }
  }

  /**
   * Get active interview by ID associated with a directors request
   */
  async getInterviewById(req, res) {
    this.validate(req);

    const interview = await Interview.findOne({
      _id: req.params.interviewId,
      active: true
    }).populate('request');
    if (!interview || interview.request.user.id !== req.user.id)
      throw new NotFoundException('Данного собеседования нет в системе');

    return res.status(200).json(interview.depopulate('request'));
  }

  /**
   * Set interview result
   */
  async setInterviewResult(req, res) {
    this.validate(req);

    const interview = await Interview.findOne({
      _id: req.params.interviewId,
      active: true
    }).populate('request');
    if (!interview || interview.request.user.id !== req.user.id)
      throw new NotFoundException('Данного собеседования нет в системе');

    interview.set('result', req.body.result);
    interview.set('comment', req.body.comment);

    await interview.save();

    return res.status(200).json(interview.depopulate('request'));
  }

  async getInterviewCount(req, res) {
    const requests = await Request.find({
      user: req.user.id,
      status: 'processing'
    });

    const interviews = await Interview.find({
      active: true,
      request: { $in: requests.map(r => r.id) }
    });

    return res.status(200).json(interviews.length);
  }
}

module.exports = new DirectorController();
