const BaseController = require('./BaseController');
const User = require('../models/User');
const userExists = require('../helpers/userExists');

class AuthController extends BaseController {
  constructor() {
    super();

    this.register = this.register.bind(this);
    this.login = this.login.bind(this);
  }

  /**
   * Register a user
   */
  async register(req, res, next) {
    this.validate(req);

    const user = new User(
      this.only(req.body, [
        'firstName',
        'lastName',
        'middleName',
        'email',
        'password',
        'role'
      ])
    );

    try {
      await user.save();
    } catch (err) {
      return next(err);
    }

    return res.status(201).json({ user });
  }

  /**
   * Login a user
   */
  async login(req, res, next) {
    this.validate(req);

    const user = await userExists(req.body.email);

    if (!user || (user && !(await user.comparePasswords(req.body.password)))) {
      return res
        .status(400)
        .json({ errors: { email: 'Неправильный логин или пароль' } });
    }

    const token = user.createToken();

    return this.sendToken(req, res, token, user);
  }

  /**
   * Send response with a new token
   */
  sendToken(req, res, token, user) {
    const expires =
      Date.now() + +process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000;
    const bearerToken = `Bearer ${token}`;

    return res.status(200).json({ token: bearerToken, expires, user });
  }

  /**
   * Logout a user
   */
  async logout(req, res, next) {
    return res
      .clearCookie('jwt', {
        httpOnly: true
      })
      .status(200)
      .json({ message: 'Вы успешно вышли из системы' });
  }

  /**
   * Get current user info
   */
  async getCurrentUser(req, res, next) {
    const user = await User.findById(req.user._id);

    return res.status(200).json({ user });
  }
}

module.exports = new AuthController();
