const { validationResult } = require('express-validator');

const { ValidationException } = require('../errors/Validation.exception');

class BaseController {
  /**
   * Filter object's properties
   */
  only(originalObj, includedProps) {
    const obj = {};

    for (let prop of includedProps) {
      if (typeof originalObj[prop] !== 'undefined')
        obj[prop] = originalObj[prop];
    }

    return obj;
  }

  /**
   * Validate incoming requests body
   */
  validate(req) {
    const result = validationResult(req);

    if (!result.isEmpty()) {
      const errors = this.getErrorsObject(result.mapped());

      throw new ValidationException(errors);
    }
  }

  /**
   * Create an object for errors in a format of {'errorField': 'errorMessage'}
   */
  getErrorsObject(errors) {
    const obj = {};

    for (let key in errors) {
      obj[key] = errors[key].msg;
    }

    return obj;
  }
}

module.exports = BaseController;
