module.exports = (req, res, next) => {
  const bearerToken = req.headers.authorization;

  if (bearerToken) {
    return res.sendStatus(403);
  }

  next();
};
