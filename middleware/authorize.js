const jwt = require('jsonwebtoken');

const User = require('../models/User');

module.exports = async (req, res, next) => {
  let token = req.headers.authorization;

  if (!token) {
    return res.sendStatus(401);
  }

  token = token.split(' ')[1];

  try {
    const decoded = await jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id);

    if (!user) {
      return res
        .status(400)
        .clearCookie('jwt', {
          httpOnly: true
        })
        .json({ message: 'Такого пользователя нет в системе' });
    }

    req.user = user;
  } catch (err) {
    return next(err);
  }

  next();
};
