/**
 * Array defining roles in system
 */
exports.roles = ['director', 'recruiter'];
