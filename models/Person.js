const { Schema, model } = require('mongoose');
const { ObjectId } = require('mongoose').SchemaTypes;

const personSchema = new Schema({
  firstName: String,
  lastName: String,
  middleName: String,
  age: Number,
  citizenship: String,
  phone: String,
  comment: String,
  employed: {
    type: Boolean,
    default: false,
  },
  job: {
    company: {
      type: ObjectId,
      ref: 'Company',
    },
    position: String,
    schedule: String,
    rate: Number,
  },
  resource: String,
});

personSchema.set('timestamps', true);

personSchema.method('toJSON', function() {
  const { _id, __v, updatedAt, ...rest } = this.toObject();

  return { id: _id, ...rest };
});

const Person = model('Person', personSchema);

module.exports = Person;
