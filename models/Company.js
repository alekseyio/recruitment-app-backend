const { Schema, model } = require('mongoose');

const Employee = require('./Employee').schema;

/**
 * Company model
 */
const companySchema = new Schema({
  brand: String,
  objectNumber: Number,
  subway: String,
  subwayLine: Number,
  address: String,
  employees: [Employee],
});

companySchema.set('timestamps', true);

companySchema.method('toJSON', function() {
  const { _id, __v, createdAt, updatedAt, ...rest } = this.toObject();

  return { id: _id, ...rest };
});

const Company = model('Company', companySchema);

module.exports = Company;
