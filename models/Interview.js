const { Schema, model } = require('mongoose');
const { ObjectId } = require('mongoose').SchemaTypes;

const interviewSchema = new Schema({
  request: {
    type: ObjectId,
    ref: 'Request'
  },
  person: {
    type: ObjectId,
    ref: 'Person'
  },
  date: Date,
  active: {
    type: Boolean,
    default: true
  },
  result: String,
  comment: String
});

interviewSchema.set('timestamps', true);

interviewSchema.method('toJSON', function() {
  const { _id, __v, ...rest } = this.toObject();

  return { id: _id, ...rest };
});

const Interview = model('Interview', interviewSchema);

exports.Interview = Interview;
