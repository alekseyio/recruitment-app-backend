const { Schema, model } = require('mongoose');
const { ObjectId } = require('mongoose').SchemaTypes;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * User definition
 */
const userSchema = new Schema({
  firstName: String,
  lastName: String,
  middleName: String,
  email: String,
  password: String,
  role: String,
  company: {
    type: ObjectId,
    ref: 'Company'
  }
});

/**
 * Set createdAt and updatedAt timestamps
 */
userSchema.set('timestamps', true);

/**
 * Remove unnecessary fields when converted to JSON
 */
userSchema.method('toJSON', function() {
  const { _id, password, createdAt, updatedAt, __v, ...obj } = this.toObject();

  return { id: _id, ...obj };
});

/**
 * Password hashing on save if password was modified
 */
userSchema.pre('save', async function(next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(
      this.password,
      parseInt(process.env.SALT_ROUNDS)
    );
  }

  next();
});

/**
 * Initially set director's company to null
 */
userSchema.pre('save', function(next) {
  if (this.isModified('role') && this.role === 'director') {
    this.company = null;
  }

  next();
});

/**
 * Populate 'company' field on user lookup
 */
userSchema.pre(/^find/, function(next) {
  this.populate({
    path: 'company',
    select: '-__v -createdAt -updatedAt -_id'
  });

  next();
});

/**
 * Compare hashed and provided passwords
 */
userSchema.methods.comparePasswords = async function(password) {
  return await bcrypt.compare(password, this.password);
};

/**
 * Create a JWT
 */
userSchema.methods.createToken = function() {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });
};

/**
 * Add company to director
 */
userSchema.methods.addCompany = function(companyId) {
  this.company = companyId;
  this.save();
};

const User = model('User', userSchema);

module.exports = User;
