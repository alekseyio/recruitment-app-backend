const { Schema, model } = require('mongoose');
const { ObjectId } = require('mongoose').SchemaTypes;

const requestSchema = new Schema({
  title: String,
  position: String,
  schedule: String,
  rate: Number,
  qty: String,
  comment: String,
  status: {
    type: String,
    default: 'new',
  },
  user: {
    type: ObjectId,
    ref: 'User',
  },
});

requestSchema.set('timestamps', true);

requestSchema.method('toJSON', function() {
  const { _id, __v, updatedAt, ...rest } = this.toObject();

  return { id: _id, ...rest };
});

requestSchema.pre(/^find/, function(next) {
  this.populate({
    path: 'user',
    select: '-__v -password -createdAt -updatedAt',
  });

  next();
});

const Request = model('Request', requestSchema);

module.exports = Request;
