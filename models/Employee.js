const { Schema, model } = require('mongoose');
const { ObjectId } = require('mongoose').SchemaTypes;

const employeeSchema = new Schema({
  company: {
    type: ObjectId,
    ref: 'Company',
  },
  person: {
    type: ObjectId,
    ref: 'Person',
  },
  position: String,
  schedule: String,
});

employeeSchema.set('timestamps', true);

employeeSchema.method('toJSON', function() {
  const { _id, __v, updatedAt, ...rest } = this.toObject();

  return { id: _id, ...rest };
});

const Employee = model('Employee', employeeSchema);

module.exports = Employee;
